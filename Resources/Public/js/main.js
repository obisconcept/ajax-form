var clickedButton = null;

$(document).ready(function() {

    initAjax();

});

function initAjax() {

    // Load ajax form
    $.each($('div[data-ajax="ajax-loaded-form"]'), function (idx, ajaxForm) {
      
      var formIdentifier = $(ajaxForm).attr('data-identifier');
      var presetName = $(ajaxForm).attr('data-presetName');
      
      // This route must be configured in the /Configuration/Routes.yaml for the project
      var formAjaxUrl = location.protocol + '//' + location.host + '/form/'  + $('html')[0].lang + '/' + formIdentifier + '/' + presetName;
      
      // Delegate the submit form event to the persistent ajax container
      $(ajaxForm).off('submit').on('submit', 'form', function(e) {
        
        var formObj = $(this);
        var formURL = formObj.attr('action');
        var formData = new FormData(this);
        
        $(this).find('button[type="submit"][clicked="true"]').each(function() {
          formData.append($(this).attr('name'), $(this).attr('value'));
        });
        
        $.ajax({
          
          url: formURL,
          type: 'POST',
          data: formData,
          mimeType: 'multipart/form-data',
          contentType: false,
          cache: false,
          processData: false,
          beforeSend: function () {
            
            $(ajaxForm).find('.ajax-content').replaceWith('<i class=" ajax-content fa fa-spinner fa-pulse"></i>');
            
          },
          success: function (data) {
            
            // Replace the form with the replied content
            $(ajaxForm).find('.ajax-content').replaceWith(data);
            registerButtonClickedStateEventListener(ajaxForm);
            
          }
          
        });
        
        //Prevent the browser from submitting the form and cause page reload
        e.preventDefault();
        
      });
      
      $(ajaxForm).load(formAjaxUrl + ' .ajax-content', function () {
        
        $(this).css("min-height", $(this).css("height"));
        registerButtonClickedStateEventListener(ajaxForm);
        
      });
      
    });
    
    function registerButtonClickedStateEventListener(ajaxForm) {
      var buttons = $(ajaxForm).find('button[type="submit"]');
      buttons.each(function () {
        $(this).on('click', function (event) {
          $(buttons).removeAttr('clicked');
          $(this).attr('clicked', 'true');
        })
      })
    }

}
