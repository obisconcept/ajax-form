# Neos CMS - AJAX form plugin
A small plugin to submit forms with AJAX.

## Installation
Add the package in your site package composer.json
```
"require": {
    "obisconcept/ajax-form": "1.1.*"
}
```

Add in your Routes.yaml of your base distribution
```
-
  name: 'AjaxForm'
  uriPattern: '<AjaxFormSubroutes>'
  subRoutes:
    'AjaxFormSubroutes':
      package: 'ObisConcept.AjaxForm'
```
Note: Add the sub routes before the neos sub routes

Configure your form with TypoScript
```
prototype(ObisConcept.AjaxForm:AjaxForm) {
    
    presetName = 'contact'

}
```
The presetName is the preset of your form, which uses the ajax functionality

Remove jQuery, if you already have integrated jQuery in your site
```
prototype(TYPO3.Neos:Page) {
    
    ajaxFormjQueryJs >

}
```

## Version History

### Changes in 1.1.2
- Added bugfix for IE

### Changes in 1.1.1
- Changed README.md

### Changes in 1.1.0
- Locale integration

### Changes in 1.0.7
- Bugfix

### Changes in 1.0.6
- Added bugfix for backend

### Changes in 1.0.5
- Bugfixing and updated README.md

### Changes in 1.0.4
- Added default jQuery AJAX request

### Changes in 1.0.3
- Bugfixing

### Changes in 1.0.2
- Added translations

### Changes in 1.0.1
- Added TypoScript and translations auto include

### Changes in 1.0.0
- First version of the plugin